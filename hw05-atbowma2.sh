#!/usr/bin/bash
#Getting input from the user and storing it in the temp variable
read -p "Please enter a tempertaure to convert (Include F,C,K at the end. Example: 24F or 24C): " temp
#First if statement if the user enters Farenheit
if [[ "$temp" =~ [0-9]"F" ]]
then
	#Made a new variable to store just the number without the letter to do calculations.
	newtemp=$(echo $temp | sed 's/[^0-9]*//g')
	#Made a new variable that holds the calculations. The scale is set to two to determine how many decimal places to interpret. Pipe to bc to handle decimals.
	celsius=$(echo "scale=2;(5/9) * ($newtemp - 32)" | bc)
	#The formula for Celsius > Kelvin is just to add 273.15 so I went with that. Once again pipe to bc to handle the decimals
	kelvin=$(echo "scale=2;$celsius + 273.15" | bc)
	#Return the value in x.xx format
	echo Celsius = "$celsius"C
	echo Kelvin = "$kelvin"K
fi
#Second if statement if users enters Celsius
if [[ "$temp" =~ [0-9]"C" ]]
then
	newtemp=$(echo $temp | sed 's/[^0-9]*//g')
	farenheit=$(echo "scale=2;((9/5) * $newtemp) + 32" | bc)
	kelvin=$(echo "scale=2;$newtemp + 273.15" | bc)
	echo Farenheit = "$farenheit"F
	echo Kelvin = "$kelvin"K
fi
#Third if statement if user enters Kelvin
if [[ "$temp" =~ [0-9]"K" ]]
then
	newtemp=$(echo $temp | sed 's/[^0-9]*//g')
	celsius=$(echo "scale=2;$newtemp - 273.15" | bc)
	farenheit=$(echo "scale=2;((9/5) * $celsius) + 32" | bc)
	echo Celsius = "$celsius"C
	echo Farenheit = "$farenheit"F
fi


