#!/usr/bin/bash
#Ask to see if user has a folder already
read -p "Do you have a reminder file? (y/n)" remind
#If they answer n then proceed to create a newly created txt file
if [[ "$remind" = "n" ]]
then
	echo "Newly created reminder folder" >> reminder.txt
fi
#Ask for the date the reminder will be for
read -p "What date will this be for? (Day, Month, Year):" thedate
echo $thedate >> reminder.txt
#Ask for the reminder they want to insert
read -p "What is the reminder?" msg
#Append the message to the txt file
echo "$msg" >> reminder.txt
read -p "Anymore reminders for this day? (y/n)" hi
if [ $hi = "y" ]; then
	read -p "What is the reminder?" msg
	echo "$msg" >> reminder.txt
fi
cat reminder.txt
